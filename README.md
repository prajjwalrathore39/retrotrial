MarsRealEstate - Starter Code
==============================

The module has been developed from almost scratch using the codelabs resources. It is well documented in this repository and does not need any further assistance to understand or get any line of code. The comments provided there are exhaustive and extensive so this repo can be considered for quick revisions of concepts of **RecyclerView** and obviously the ones mentioned below. 

The master branch consists of the complete code of the result of the following three codelabs

Info.
------------
It has been developed as a result of following three sequential codelabs
* [Getting Data From Internet](https://codelabs.developers.google.com/codelabs/kotlin-android-training-internet-data/#0)
* [Loading and displaying images from the internet](http://codelabs.developers.google.com/codelabs/kotlin-android-training-internet-images/#0)
* [Filtering and detail views with internet data](http://codelabs.developers.google.com/codelabs/kotlin-android-training-internet-filtering/#0)

MarsRealEstate is a demo app that shows available properties for sale and for rent on Mars.
The property data is stored on a Web server as a REST web service.  This app demonstrated
the use of [Retrofit](https://square.github.io/retrofit/) to make REST requests to the 
web service, [Moshi](https://github.com/square/moshi) to handle the deserialization of the 
returned JSON to Kotlin data objects, and [Glide](https://bumptech.github.io/glide/) to load and 
cache images by URL.  

The app also leverages [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel),
[LiveData](https://developer.android.com/topic/libraries/architecture/livedata), 
[Data Binding](https://developer.android.com/topic/libraries/data-binding/) with binding 
adapters, and [Navigation](https://developer.android.com/topic/libraries/architecture/navigation/) 
with the SafeArgs plugin for parameter passing between fragments.

# The module has been marked final and complete as per the codelabs progress of the sequence
