package com.example.android.marsrealestate

//all enums go here

enum class MarsApiStatus{ LOADING, ERROR, DONE}

// enum class for filtering the responses.
// Define constants that match the query values the web service expects.
enum class MarsApiFilter(val value: String) {
  SHOW_RENT("rent"),
  SHOW_BUY("buy"),
  SHOW_ALL("all")  /** used in [OverviewViewModel] */
}
