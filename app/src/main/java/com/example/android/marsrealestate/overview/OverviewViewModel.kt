/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.android.marsrealestate.overview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.MarsApiFilter
import com.example.android.marsrealestate.MarsApiFilter.SHOW_ALL
import com.example.android.marsrealestate.MarsApiStatus
import com.example.android.marsrealestate.MarsApiStatus.DONE
import com.example.android.marsrealestate.MarsApiStatus.ERROR
import com.example.android.marsrealestate.MarsApiStatus.LOADING
import com.example.android.marsrealestate.network.MarsApi
import com.example.android.marsrealestate.network.MarsProperty
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

/**
 * The [ViewModel] that is attached to the [OverviewFragment].
 */
class OverviewViewModel : ViewModel() {

  // The internal MutableLiveData String that stores the most recent response
  private val _status = MutableLiveData<MarsApiStatus>()
  private val _props = MutableLiveData<List<MarsProperty>>()
  private val _navigateToSelectedProp = MutableLiveData<MarsProperty>()

  /** This @param-> props is a getter for @param->_props(which is mutable in this ViewModelScope).
   *  This getter returns something that is not exact like @param->props and mutable but instead, it
   *  returns the immutable form of @param->_props
   * */
  val props: LiveData<List<MarsProperty>>
    get() = _props

  val status: LiveData<MarsApiStatus>
    get() = _status

  val navigateToSelectedProp: LiveData<MarsProperty>
    get() = _navigateToSelectedProp

  private val viewModelJob = Job()

  /** The Dispatchers.Main dispatcher uses the UI thread for its work.
   *  Because Retrofit does all its work on a background thread,
   *  there's no reason to use any other thread for the scope.
   *  This allows us to easily update the value of the MutableLiveData
   *  when we get a result.
   *  */
  private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

  /** Call getMarsRealEstateProperties() on init to display status immediately. */
  init {
    getMarsRealEstateProperties(SHOW_ALL)
  }

  /** Sets the value of the status LiveData to the Mars API status */
  private fun getMarsRealEstateProperties(filter: MarsApiFilter) {

    /** To use the Deferred object that Retrofit returns for the network task,
     *  we have to be inside a coroutine, so here launch the coroutine we just created.
     *  We're still executing code on the main thread,
     *  but now we're letting coroutines manage concurrency.
     *  */
    coroutineScope.launch {

      /** Calling await() on the Deferred object returns the result from
       *  the network call when the value is ready.
       *  The await() method is non-blocking, so the Mars API service retrieves
       *  the data from the network without blocking the
       *  current thread—which is important because we're in the scope of the
       *  UI thread. Once the task is done, the code continues executing from where it left off.
       *  This is inside the try {} to catch exceptions.
       *  */

      val getPropsDeferred = MarsApi.retrofitInstance.getProps(filter.value)
      try {
        _status.value = LOADING
        val listResult = getPropsDeferred.await()
//        _response.value = " Success:: ${listResult.size} number of Mars Props retrieved!!! "
        _status.value = DONE
        _props.value = listResult

      } catch (e: Exception) {
        e.printStackTrace()
        _status.value = ERROR
        _props.value = ArrayList()

      }
    }
  }

  /** Loading data should stop when the ViewModel is destroyed,
   *  because the OverviewFragment that uses this ViewModel will be gone.
   *  To stop loading when the ViewModel is destroyed,
   *  override onCleared() to cancel the job.
   *  */

  override fun onCleared() {
    super.onCleared()
    viewModelJob.cancel()
  }

  /** Method to fill the filter query in the request */
  fun updateFilter(filter: MarsApiFilter) {
    getMarsRealEstateProperties(filter)
  }

  fun displayPropDetails(marsProperty: MarsProperty) {
    _navigateToSelectedProp.value = marsProperty
  }

  /** To mark the navigation state to complete, and to avoid the
   *  navigation being triggered again when the user
   *  returns from the detail view.
   *  */
  fun displayPropDetailsCompleted() {
    _navigateToSelectedProp.value = null
  }

}
