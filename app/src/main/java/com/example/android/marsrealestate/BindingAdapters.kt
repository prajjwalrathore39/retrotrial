/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.android.marsrealestate

import android.view.View
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.android.marsrealestate.MarsApiStatus.DONE
import com.example.android.marsrealestate.MarsApiStatus.ERROR
import com.example.android.marsrealestate.MarsApiStatus.LOADING
import com.example.android.marsrealestate.R.drawable
import com.example.android.marsrealestate.network.MarsProperty
import com.example.android.marsrealestate.overview.PhotoGridAdapter

/** The @BindingAdapter annotation tells data binding
 *  to bind/execute this binding adapter to an XML item
 *  that has the imageUrl attribute.
 *  */
@BindingAdapter("imageUrl")
fun bindImage(
  imgView: ImageView,
  imgUrl: String?
) {
  imgUrl?.let {

    /** We want the final Uri object to use the HTTPS scheme,
     * because the server we pull the images from requires that scheme.
     * To use the HTTPS scheme, append buildUpon.scheme("https") to the toUri builder.
     * The toUri() method is a Kotlin extension function from the Android KTX core library,
     * so it just looks like it's part of the String class.
     * */

    val imgUri = imgUrl.toUri()
      .buildUpon()
      .scheme("https")
      .build()

    /** Glide.with() loads the image from the Uri object into to the @param->ImageView */
    Glide.with(imgView.context)
      .load(imgUri)
      .apply(requestOptions())
      .into(imgView)
  }
}

private fun requestOptions() = RequestOptions().placeholder(drawable.loading_animation)
  .error(drawable.ic_connection_error)

/** For binding data in the recyclerView */
@BindingAdapter("listData")
fun bindRecyclerView(
  recyclerView: RecyclerView,
  data: List<MarsProperty>?
) {
  val adapter = recyclerView.adapter as PhotoGridAdapter
  adapter.submitList(data)
}

/** For binding image loading status */
@BindingAdapter("marsApiStatus")
fun bindStatus(
  statusImageView: ImageView,
  status: MarsApiStatus?
) {

  when (status) {

    LOADING -> {
      statusImageView.visibility = View.VISIBLE
      statusImageView.setImageResource(R.drawable.loading_animation)
    }

    ERROR-> {
      statusImageView.visibility=View.VISIBLE
      statusImageView.setImageResource(drawable.ic_connection_error)
    }

    DONE ->{
      statusImageView.visibility=View.GONE
    }
  }

}