/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.android.marsrealestate.overview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.marsrealestate.databinding.GridViewItemBinding
import com.example.android.marsrealestate.network.MarsProperty
import com.example.android.marsrealestate.overview.PhotoGridAdapter.MarsPropertyViewHolder

class PhotoGridAdapter(private val onClickListener: OnClickListener) : ListAdapter<MarsProperty, MarsPropertyViewHolder>(
  DiffCallback
) {

  /** GridViewItemBinding variable is needed for binding the MarsProperty to the layout,
   *  so pass the variable into the MarsPropertyViewHolder. Because the
   *  base ViewHolder class requires a view in its constructor,
   *  I passed in the binding root view.
   *  */

  class MarsPropertyViewHolder(private var binding: GridViewItemBinding) : RecyclerView.ViewHolder(
    binding.root
  ) {

    fun bind(marsProperty: MarsProperty) {
      binding.properties = marsProperty
      binding.executePendingBindings() //executePendingBindings() causes the update to execute immediately.
    }

  }

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): MarsPropertyViewHolder {

    return MarsPropertyViewHolder(GridViewItemBinding.inflate(LayoutInflater.from(parent.context)))

  }

  override fun onBindViewHolder(
    holder: MarsPropertyViewHolder,
    position: Int
  ) {
    val marsProperty = getItem(position)
    holder.itemView.setOnClickListener { this.onClickListener.onClick(marsProperty) }
    holder.bind(marsProperty)
  }

  /** For the areItemsTheSame() method, use the
   *  Kotlin's referential equality operator (===),
   *  which returns true if the object references for
   *  oldItem and newItem are the same.
   */
  companion object DiffCallback : DiffUtil.ItemCallback<MarsProperty>() {
    override fun areItemsTheSame(
      oldItem: MarsProperty,
      newItem: MarsProperty
    ): Boolean {
      return oldItem === newItem
    }

    override fun areContentsTheSame(
      oldItem: MarsProperty,
      newItem: MarsProperty
    ): Boolean {
      return oldItem.id == newItem.id
    }

  }

  /** A custom OnClickListener class that takes a lambda with a marsProperty parameter.
   *  Inside the class, define an onClick() function that is set to the lambda parameter.
   */
  class OnClickListener(val clickListener: (marsProp: MarsProperty) -> Unit) {
    fun onClick(marsProperty: MarsProperty) = clickListener(marsProperty)
  }

}